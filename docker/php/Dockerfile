FROM php:8.2-fpm

ARG UID
ARG GID

ENV UID=${UID}
ENV GID=${GID}

WORKDIR /var/www/simple-cms-ddd

RUN addgroup -gid ${GID} --system app
RUN adduser --system --ingroup app -shell /bin/sh --uid ${UID} --disabled-password app

RUN sed -i "s/user = www-data/user = app/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = app/g" /usr/local/etc/php-fpm.d/www.conf

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get update && apt-get install -y  \
    git zip supervisor

RUN install-php-extensions intl opcache pgsql pdo_pgsql zip xdebug pdo_mysql mysqli apcu

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

COPY docker/php/conf.d/xdebug.ini "${PHP_INI_DIR}"/conf.d

EXPOSE 9000
CMD ["php-fpm"]
